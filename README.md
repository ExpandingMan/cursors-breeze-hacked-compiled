# Breeze Hacked Cursors, Compiled (White)
The "Breeze Hacked" cursor theme done in a neutral black-and-white.  This is the compiled version,
for modifications see the [original
repository](https://github.com/clayrisser/breeze-hacked-cursor-theme).

## Installation
Clone to `~/.icons` as `Breeze_Hacked`.  In most cases, it is sufficient to modify `~/.icons/default/index.theme` to
contain
```
[Icon Theme]
Inherits=Breeze_Hacked
```
and `~/.config/gtk-3.0/settings.ini` to contain
```
[Settings]
gtk-cursor-theme-name=Breeze_Hacked
```
(In both cases you should replace existing values).

See [the Arch linux wiki](https://wiki.archlinux.org/title/Cursor_themes) for
comprehensive installation instructions.

## Preview
[![preview](preview.png)](preview/)

## Credits
The original repository cna be found
[here](https://github.com/clayrisser/breeze-hacked-cursor-theme).

## License
[GPL 2.0 License](LICENSE)
